api = 2
core = 7.x

; Drupal
includes[] = drupal-org-core.make

; Panopoly
projects[panopoly][type] = profile
projects[panopoly][download][type] = git
projects[panopoly][download][branch] = 7.x-1.x
projects[panopoly][download][url] = git.drupal.org:sandbox/arshad/2045985.git
